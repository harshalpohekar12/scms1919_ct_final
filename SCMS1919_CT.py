import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt 

if __name__ == "__main__":
    np.random.seed(45)
    p = [0.3, 0.7]
    runSize = 10#0000
    for prob in p:
        attempts = []
        for i in range(runSize):
            output = 1
            while not (np.random.binomial(1, prob)):
                output += 1
            attempts.append(output)
        plt.hist(attempts)
        plt.title("Histogram of attempts for probability %f" % (prob))
        plt.show()
    
